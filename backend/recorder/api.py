from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.core.files.base import File

# Create your views here.

from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import status
from recorder.models import Video
from recorder.serializer import VideoSerializer
from recorder.helpers import GoogleStorage


def web_cam(request):
    return render(request, 'home.html')


@api_view(['GET'])
def video_list(request):
    videos = Video.objects.all()
    serializer = VideoSerializer(videos, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def video_detail(request, pk):
    try:
        video = Video.objects.get(pk=pk)
    except Video.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    serializer = VideoSerializer(video)
    return Response(serializer.data)


class VideoViewSet(viewsets.ModelViewSet):
    queryset = Video.objects.all()
    serializer_class = VideoSerializer
    permission_classes = []
    
    def create(self, request):
        video = request.data['videoFile']
        request.data['video_file'] = File(video)
        print(request.data)
        serializer = VideoSerializer(data=request.data)
        if serializer.is_valid():
            file_path = GoogleStorage.get_file_path(serializer.validated_data)
            serializer.validated_data['video_file'].name = file_path
            video = serializer.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(
                {"status": "error", "error": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        print(request)
        print(pk)
        video = get_object_or_404(Video, id=pk)
        print(video.video_file_path)
        response = HttpResponse(video.video_file_path)
        return response

    def delete(self, request, id = None):
        book_id = get_object_or_404(Video, id=id)
        book_id.delete()
        return Response({"video deleted"})