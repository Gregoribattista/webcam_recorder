from datetime import datetime
from django.core.files.storage import default_storage
from webcam_recorder import settings
from django.core import signing


from django.conf import settings


class GoogleStorage:
    """Google cloud storage handler"""

    def get_file_path(data):
        timestamp = datetime.strftime(datetime.now(), '%Y%m%d%H%M%S')
        name_folder = f'{data["user_id"]}_{data["order_id"]}'
        # timestamp_userID_orderID
        file_full_path = settings.GS_PATH_VIDEOS + f'{name_folder}/{timestamp}_{name_folder}.mp4'
        return file_full_path

    def get_file(file_path):
        # encoded_name = signing.loads(data['name'])
        file = default_storage.open(file_path, 'r')
        file_full_path = settings.GS_PATH_VIDEOS + file.name
        return file_full_path