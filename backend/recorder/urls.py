from django.urls import path, include
from rest_framework import routers

from recorder.api import VideoViewSet

router = routers.DefaultRouter()
router.register(r'^video', VideoViewSet)

urlpatterns = [
    path('', include(router.urls)),
]