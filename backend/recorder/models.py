from django.db import models

class Video(models.Model):
    title = models.CharField(max_length=100)
    user_id = models.TextField(blank=True)
    order_id = models.TextField(blank=True)
    upload_date = models.DateField(auto_now_add=True)
    video_file = models.FileField()