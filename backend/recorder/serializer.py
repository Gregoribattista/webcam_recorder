from rest_framework import serializers
from recorder.models import Video


class VideoSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=100)
    userId = serializers.CharField(max_length=100, source="user_id")
    orderId = serializers.CharField(max_length=100, source="order_id")
    upload_date = serializers.DateField(required=False)
    video_file = serializers.FileField()

    def create(self, validated_data):
        print(validated_data['video_file'])
        return Video.objects.create(
            user_id = validated_data['user_id'],
            order_id = validated_data['order_id'],
            title = validated_data['title'],
            video_file = validated_data['video_file'],
        )
    

    