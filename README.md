# Webcam recorder


### Before start 📋

Before starting the installation, it's recommended to **create a virtual environment**. That will create a space for the project to have its own dependencies, without intervening from the dependencies every other project in your system has.\
This enviroment is where you are going to install the requirements that make the project works correctly.\
\
To install, you have to **run these commands** in the Terminal:

> $`virtualenv venv                  # this create the environment called "venv"`\
$`source venv/bin/activate                # this will activated the environment`


## Clone the repository :inbox_tray:

You can download the repository following these commands

> $`git init`\
$`git clone https://gitlab.com/Gregoribattista/webcam_recorder.git`



### Install 🔧

First of all, you need to install the requirements for the project to work properly.

Run in the same terminal

> $`cd webcam_recorder`\
$`pip install -r requirements.txt`


## Run to the WebCam🚀

Follow in order the next steps to execute the project.

1. Execute the migrations.
> $`python manage.py migrate`
2. Now you can run the server.
> $`python manage.py runserver`\
> If the command return "Error: That port is already in use." change the port 8080 to another until it works.


### 2. Install front-end project

You can download the repository following these commands

$`cd ..`\
$`git clone https://gitlab.com/Gregoribattista/webcam_frontend.git`\
$`cd webcam_frontend`\
$`yarn install`\


### 3. Testing

1. Open 2 diferents terminal in the folder when you clone the both projects.
2. In one of them, go to the frontend repository and start up following these commands
> $`cd webcam_frontend`\
> $`yarn serve`\
3. Now the frontend server started in local host http://localhost:8080/
4. In the other terminal, start the backend server with nexts steps:
> $`cd webcam_recorder`\
> $`python manage.py runserver`\
5. Finally, you can test the conection between both servers in the frontend url.

## Softwares 🛠️

This project was created with the following software

* [Django Rest Framework](https://www.django-rest-framework.org/) - Framework
* [Python](https://www.python.org/) - Programming language
